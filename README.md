Contains data and code of the publication Acclimation of Chlamydomonas to light stress is mediated through multiple transcription factors

- **wild-type_RNA-seq_expression_matrix.tsv.gz**

    Expression matrix mapped with kallisto of 769 publicly available wild-type RNA-seq experiments mapped with kallisto on the primary transcriptome of C. reinhardtii v5.6

    The first row contains the column names "target_id" followed by the SRA accession numbers.

    columns: tpms of SRA-IDs experiments

    the column "target_id" contains gene IDs of the genome version v5.6

- **GENIE3_GRN.tsv.gz**

    Gene regulatory network of _C. reinhardtii_ inferred from wild-type_RNA-seq_expression_matrix.tsv.gz using GENIE3 and 237 transcription factors

    columns: regulator gene IDs

    column "target_id": gene IDs

- **person_cor_GRN.tsv.gz**

    Gene regulatory network of _C. reinhardtii_ inferred from wild-type_RNA-seq_expression_matrix.tsv.gz using the pearson correlation coefficient 

    columns: regulator gene IDs

    column "target_id": gene IDs

- **GO-term_enrichment_for_each_tf_GENIE3_network.tsv.gz**

    contains the GO-term enrichment of the GENIE3 network for every transcription factors for a cutoff of 0.005

- **Cr_mutant_RNA-seq.tsv.gz**

    contains tpms, counts, logFC and FDR of mutant RNA-seq of HY5, COP1 PRR9, GATA19, RKD1, TOE2, ROC40, COL2, ABI5 of _C. reinhardtii_ 


- **NPQ_measurements.tsv**

    contains NPQ NPQ_measurements of the mutants of _ppr9_, _rkd1_, _gata19_, _toe2_, _roc40_, _col2_, _abi5_

- **Photosynthesis_pathway_plot**

    contains the code to generate the photosynthesis pathway plot

- **SRA_mapping_skript**

    contains the code to download and map experiements from SRA

- **gotermenrichmentgrn**

    contains the code to calculate a GO term enrichment for eacht transcription factor of a gene regulatory network

- **DAP-seq**

    contains the DAP-seq peaks and motifs of GATA19, HY5 and RKD1
