import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, Rectangle, Ellipse, FancyBboxPatch
import pandas as pd
from sklearn import preprocessing
from matplotlib import colors
import matplotlib.transforms as transforms
import numpy
from scipy.interpolate import interp1d





file="PS_all.tsv"
PSII=pd.read_csv(file,sep="\t")

abs_cutoff=3
abs_cutoff_color=["#42cef5","#f542ef"]
PSII.loc[PSII.expression > abs_cutoff, 'expression'] = abs_cutoff
PSII.loc[PSII.expression < -abs_cutoff, 'expression'] = -abs_cutoff

x = PSII["expression"]
type(x)

abs_max=max([max(x),abs(min(x))])
abs_min_max=pd.Series([abs_max,-abs_max])




x=numpy.array(x)
xyz=numpy.append(abs_min_max,x)
list = xyz.reshape(-1,1)
print('Original List:',list)
scaler = preprocessing.MinMaxScaler(feature_range=(0,1))
normalised_expression=scaler.fit_transform(list)
PSII["expression_scaled"]=normalised_expression[2:]

cmap = matplotlib.colormaps["bwr"].reversed()
#cmap(PSII["expression"])

PSII.to_csv("PS_all_scaled.tsv",sep="\t")

fig, ax = plt.subplots()
img = plt.imshow(numpy.array([[-abs_max,abs_max]]), cmap=cmap)
img.set_visible(False)
plt.colorbar(orientation="vertical")
layer=-1000

PSII_grouped=PSII.groupby("TF")

for name,PSII_TF in PSII_grouped:
  for rows in PSII_TF.iterrows():
    print(rows)
    if pd.isnull(rows[1]["expression_scaled"]):
      color="#D3D3D3"
    else:
      if rows[1]["expression_scaled"]==1:
        color=abs_cutoff_color[0]
      elif rows[1]["expression_scaled"]==-1:
        color=abs_cutoff_color[1]
      else:
        color=cmap(rows[1]["expression_scaled"])
    if rows[1]["shape"] == "round":
      x=float(rows[1]["x"])
      y=float(rows[1]["y"])
      width=float(rows[1]["width"])
      length=float(rows[1]["length"])
      
      shape = FancyBboxPatch((x, y), width, length, boxstyle="round,pad=0,rounding_size="+str(rows[1]["corner"]), edgecolor='black', facecolor=color)
      angle=rows[1]["angle"]
      rotation = transforms.Affine2D().rotate_deg_around(x+0.5*width,y+0.5*length, angle)
      shape.set_transform(rotation + ax.transData)
      ax.add_patch(shape)
      
    if rows[1]["shape"] == "ellipse":
      x=float(rows[1]["x"])
      y=float(rows[1]["y"])
      width=float(rows[1]["width"])
      length=float(rows[1]["length"])
      
      shape = Ellipse((x, y), width=width, height=length, angle=rows[1]["angle"], facecolor=color, edgecolor='black', linewidth=1)
      ax.add_patch(shape)
      
    if rows[1]["shape"] == "polygon":
      layer+=1
      x = rows[1]["x"].split(";")
      y = rows[1]["y"].split(";")
      
      #x = "256;256;256;257;278;295;295;287;270;249;246;244;244;252;257;256".split(";")
      #y = "296;304;318;331;345;357;369;373;367;358;325;290;256;248;256;296".split(";")
      y_=[]
      for i in y:
        y_.append(float(i))
      
      t = numpy.arange(len(x))
      ti = numpy.linspace(0, t.max(), 10 * t.size)
  
      xi = interp1d(t, x, kind='cubic')(ti)
      yi = interp1d(t, y_, kind='cubic')(ti)
  
      ax.fill(xi, yi,colors.to_hex(color),zorder=layer+1)
  
      ax.plot(xi, yi,"black",linewidth=2,zorder=layer)
      
  

  ax.set_xlim(0, 614*1.3)
  ax.set_ylim(0, 480*1.3)
  plt.show()
  plt.savefig("PS_"+name+".svg",transparent=True)

