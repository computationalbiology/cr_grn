Edit these lines in Photosynthesis_pathway_plot.py


# input file

file="PS_all.tsv"



PSII=pd.read_csv(file,sep="\t")

# cutoff of heatmap


abs_cutoff=3


# color if value if cutoff is exceeded


abs_cutoff_color=["#42cef5","#f542ef"]



# add your own data into PS_all.tsv


- In the columns:
    TF: experiment_name (also output file)
	expression: value you want to in the heatmap for the gene in the column Gene_id
