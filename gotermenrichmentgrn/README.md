# GoTermEnrichmentGRN

Modified topGo to perform a gotermenrichment for each transcriptonfactor for a gene regulatory network

This script is yet not tested

you need to install topGo from bioconductor:

if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("topGO")


change inputs in python script

you need to modify these lines:

species="Chlamydomonas_reinhartii"  ### change to your species

network_cutoff=0.005

network_path="GRN.RDS"  ### network exists -> enter path here

go_term_map_path="gotermCR.map"  ### the file containing the gene IDs plus GOterm IDs

output_file_path="/path/to/output_file" 



run the python script

test one of the R scripts

run submitall.sh

wait for the jobs to finish

run puzzle_goterms.R


