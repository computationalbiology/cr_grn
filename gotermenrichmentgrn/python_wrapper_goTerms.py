import os

file=open("gotermenrichmentgrn/goterm_enrichment_for_each_tf.R","r")
x=file.read()
file.close()

n_cors=26

species="Chlamydomonas_reinhartii"  ### change to your species
network_cutoff=0.005
network_path="/mnt/volume/janosh_RNA_seq/Chlamy_GRNM_onlyCRTF.rds"  ### network exists -> enter path here
go_term_map_path="/mnt/volume/janosh_RNA_seq/gen2Go_Cre_Atblastp_20190509_comunities.map"  ### the file containing the gene IDs plus GOterm IDs
output_file_path="/mnt/volume/janosh_RNA_seq/gotermenrichmentgrn" ### where i want it to be saved (kein name, nur ordnerpfad mit slash), actually a folder path, not file
file=open("submit.sh","w")
file.close()
file=open("submit.sh","a")
for i in range(1,n_cors+1):
  print(i)
  y=x.replace("{n}",str(n_cors))
  y=y.replace("{thread}",str(i))
  y=y.replace("{Network_path}",network_path)
  y=y.replace("{species}",species)
  y=y.replace("{Network_cutoff}",str(network_cutoff))
  y=y.replace("{goterm_map_path}",go_term_map_path)
  y=y.replace("{output_file}",output_file_path)
  name="thread_"+str(i)+".R"
  thread_file=open(name,"w")
  thread_file.write(y)
  thread_file.close()
  file.write("Rscript --vanilla "+name+ "&\nsleep 2\n")
  
file.close()

if not os.path.exists(output_file_path): # create output dir
  os.makedirs(output_file_path)
