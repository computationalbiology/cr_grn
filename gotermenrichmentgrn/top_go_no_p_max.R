### May-27-2019_EF
###topGO_alt.GenTable.R
### Altered source code of topGO to modify the pvalue output.
library(DBI)
.getTermsDefinition <- function(whichTerms, ontology, numChar = 20, multipLines = FALSE) {
  
  qTerms <- paste(paste("'", whichTerms, "'", sep = ""), collapse = ",")
  retVal <- dbGetQuery(GO_dbconn(), paste("SELECT term, go_id FROM go_term WHERE ontology IN",
                                          "('", ontology, "') AND go_id IN (", qTerms, ");", sep = ""))
  
  termsNames <- retVal$term
  names(termsNames) <- retVal$go_id
  
  if(!multipLines)
    shortNames <- paste(substr(termsNames, 1, numChar),
                        ifelse(nchar(termsNames) > numChar, '...', ''), sep = '')
  else
    shortNames <- sapply(termsNames,
                         function(x) {
                           a <- strwrap(x, numChar)
                           return(paste(a, sep = "", collapse = "\\\n"))
                         })
  
  names(shortNames) <- names(termsNames)
  
  ## return NAs for the terms that are not found in the DB and make sure the 'names' attribute is as specified
  shortNames <- shortNames[whichTerms]
  names(shortNames) <- whichTerms
  
  return(shortNames)
}
### Here you can adjust the rawpvalue if your GO term enrichment is exceeding the rawpvalue cut off. Right now it is set to 1e-150.
topGO_altGenTable <- function (object, ...) 
{
  .local <- function (object, ..., orderBy = 1, ranksOf = 2, 
                      topNodes = 10, numChar = 40, format.FUN = format.pval, 
                      decreasing = FALSE, useLevels = FALSE, rawpvalue = 1e-150) 
  {
    resList <- list(...)
    if (!all(sapply(resList, is, "topGOresult"))) 
      stop("Use: topGOdata, topGOresult_1, topGOresult_2, ..., \"parameters\".")
    if (is.null(names(resList))) 
      names(resList) <- paste("result", 1:length(resList), 
                              sep = "")
    resList <- lapply(resList, score)
    if (length(resList) == 1) {
      orderBy <- ranksOf <- 1
      l <- data.frame(resList)
      names(l) <- ifelse(is.null(names(resList)), "", names(resList))
    }
    else {
      l <- .sigAllMethods(resList)
    }
    index <- order(l[, orderBy], decreasing = decreasing)
    l <- l[index, , drop = FALSE]
    if (decreasing) 
      rr <- rank(-l[, ranksOf], ties.method = "first")
    else rr <- rank(l[, ranksOf], ties.method = "first")
    whichTerms <- rownames(l)[1:topNodes]
    l <- l[whichTerms, , drop = FALSE]
    rr <- as.integer(rr[1:topNodes])
    shortNames <- .getTermsDefinition(whichTerms, ontology(object), 
                                      numChar = numChar)
    infoMat <- data.frame(`GO ID` = whichTerms, Term = shortNames, 
                          stringsAsFactors = FALSE)
    if (useLevels) {
      nodeLevel <- buildLevels(graph(object), leafs2root = TRUE)
      nodeLevel <- unlist(mget(whichTerms, envir = nodeLevel$nodes2level))
      infoMat <- data.frame(infoMat, Level = as.integer(nodeLevel))
    }
    annoStat <- termStat(object, whichTerms)
    if (ranksOf != orderBy) {
      dim(rr) <- c(length(rr), 1)
      colnames(rr) <- paste("Rank in ", ifelse(is.character(ranksOf), 
                                               ranksOf, colnames(l)[ranksOf]), sep = "")
      infoMat <- data.frame(infoMat, annoStat, rr, apply(l, 
                                                         2, format.FUN, dig = 2, eps = rawpvalue), check.names = FALSE, 
                            stringsAsFactors = FALSE)
    }
    else {
      infoMat <- data.frame(infoMat, annoStat, apply(l, 
                                                     2, format.FUN, dig = 2, eps = rawpvalue), check.names = FALSE, 
                            stringsAsFactors = FALSE)
    }
    rownames(infoMat) <- 1:length(whichTerms)
    return(infoMat)
  }
  .local(object, ...)
}
