library(tidyverse)



# you may change the path here!
# all output files from the topgo scripts need to be captured (but no other files)
df<-list.files("gotermenrichmentgrn/", pattern=":.*.Rds",recursive = T)%>%
  str_c("gotermenrichmentgrn/",.)


df_grouped<-df%>%
  enframe("number","path")%>%
  mutate(output=str_remove(path,"\\d+:\\d+_"))%>%
  group_by(output)%>%
  group_split()


for(i in df_grouped){
  z<-i%>%
    group_by(path)%>%
    group_split()
  result<-NULL
  for(j in z){
    result<-readRDS(j$path)%>%
      bind_rows(result)
  }
  distinct(result) %>%
    saveRDS(j$output)
}


unlink(df)
list.files(pattern="thread_")%>%
  unlink()
unlink("submit.sh")



#test<-readRDS(df_grouped[[1]]$output[1])%>%
#  group_by(transcription_factor)%>%
#  arrange(classicFisher)%>%
#  distinct(transcription_factor,.keep_all=T)
##
#
#test%>%
#  filter(classicFisher<1e-10)
